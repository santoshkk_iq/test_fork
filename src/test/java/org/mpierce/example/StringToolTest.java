package org.mpierce.example;

import java.util.stream.IntStream;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mpierce.example.StringTool.repeat;

public class StringToolTest {

    @Test
    public void testRepeat0() {
        assertEquals(0, repeat("foo", 0).count());
    }

    @Test
    public void testFailsWithNegativeRepeat() {
        try {
            repeat("foo", -1);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("num must be nonnegative, was -1", e.getMessage());
        }
    }

    @Test
    public void testRepeat1() {
        IntStream intStream = repeat("foo", 1);
        String result =
                getString(intStream);

        assertEquals("foo", result);
    }

    @Test
    public void testRepeatMany() {
        assertEquals("foofoofoo", getString(repeat("foo", 3)));
    }

    private static String getString(IntStream intStream) {
        return intStream
                .collect(StringBuilder::new,
                        (buf, i) -> buf.append((char) i),
                        StringBuilder::append)
                .toString();
    }
}
